# sidebar.py
#
# Copyright 2022 Ewan Higgs
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
from gi.repository import Adw, Gio, GLib, Gtk, GObject


@Gtk.Template(resource_path='/com/gitlab/ehiggs/kanban/gui/sidebar.ui')
class Sidebar(Gtk.Widget):
    __gtype_name__ = 'Sidebar'
    def __init__(self):
        print('initializing sidebar')
        self.action_group = Gio.SimpleActionGroup.new()
        move_down_action = Gio.SimpleAction(name="move-down")
        move_down_action.connect("activate", self.on_action_move_down_activated_cb)
        self.action_group.add_action(move_down_action)

        move_up_action = Gio.SimpleAction(name="move-up")
        move_up_action.connect("activate", self.on_action_move_up_activated_cb)
        self.action_group.add_action(move_down_action)

        self.main = Gtk.Template.Child('main')
        self.archive = Gtk.Template.Child('archive')
        self.listbox = Gtk.Template.Child('listbox')
        self.archive_listbox = Gtk.Template.Child('archive_listbox')

    def on_action_move_down_activated_cb(self, arg, other):
        print('on_action_move_down_activated_cb')

    def on_action_move_up_activated_cb(self, arg, other):
        print('on_action_move_up_activated_cb')

    def on_listbox_row_activated_cb(self, arg, other):
        print("listbox row activated_cb")

GObject.type_ensure(Sidebar.__gtype__)

