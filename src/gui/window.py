# window.py
#
# Copyright 2022 Ewan Higgs
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
from gi.repository import Adw, Gio, GLib, Gtk
from kanban.gui.sidebar import Sidebar


@Gtk.Template(resource_path='/com/gitlab/ehiggs/kanban/gui/window.ui')
class KanbanWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'KanbanWindow'

    workspace_layout = Gtk.Template.Child("workplace_layout")
    sidebar = Gtk.Template.Child("sidebar")
    workplace_stack = Gtk.Template.Child("workplace_stack")

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.settings = Gio.Settings(schema_id="com.gitlab.ehiggs.kanban")
        self.settings.bind("window-width", self, "default-width",
                           Gio.SettingsBindFlags.DEFAULT)
        self.settings.bind("window-height", self, "default-height",
                           Gio.SettingsBindFlags.DEFAULT)
        self.settings.bind("window-maximized", self, "maximized",
                           Gio.SettingsBindFlags.DEFAULT)


class AboutDialog(Gtk.AboutDialog):

    def __init__(self, parent):
        Gtk.AboutDialog.__init__(self)
        self.props.program_name = 'kanban'
        self.props.version = "0.1.0"
        self.props.authors = ['Ewan Higgs']
        self.props.copyright = '2022 Ewan Higgs'
        self.props.logo_icon_name = 'com.gitlab.ehiggs.kanban'
        self.props.modal = True
        self.set_transient_for(parent)
